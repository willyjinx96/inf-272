const express = require('express');
const app = express();

const dotenv = require('dotenv');
dotenv.config();

//conexión con la base de datos
const { connection } = require('../config/db');

const getUser = (request, response) => {
  connection.query('SELECT * FROM users', (error, results) => {
    if (error) throw error;
    response.status(200).json(results);
  });
};

//ruta
app.route('/users').get(getUser);

const postUser = (request, response) => {
  const { username, password } = request.body;
  connection.query(
    'INSERT INTO users(username, password) VALUES (?,?) ',
    [username, password],
    (error, results) => {
      if (error) throw error;
      response
        .status(201)
        .json({ 'Usuario añadido correctamente': results.affectedRows });
    }
  );
};

//ruta
app.route('/users').post(postUser);

const delUser = (request, response) => {
  const id = request.params.id;
  connection.query('Delete from users where id = ?', [id], (error, results) => {
    if (error) throw error;
    response.status(201).json({ 'Usuario eliminado': results.affectedRows });
  });
};

//ruta
app.route('/users/:id').delete(delUser);

module.exports = app;
