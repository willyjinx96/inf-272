
# API REST (MySQL + NodeJS)

Vamos a crear un API REST usando NodeJS junto con Express y MySQL.

## Base de Datos
Vamos a crear primero una base de datos llamada `inf272` en la que crearemos la tabla `users`:



```sql
--MY SQL
CREATE TABLE USERS (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    USERNAME VARCHAR(100) NOT NULL,
    PASSWORD VARCHAR(100)
);

INSERT INTO USERS(USERNAME, PASSWORD) VALUES ("WWW96", "12345");
INSERT INTO USERS(USERNAME, PASSWORD) VALUES ("JUAN34", "ABCDEF");
```

## CRUD

Los métodos que vamos a implementar son tres de los cuatro típicos de un CRUD (create, read, update, delete):

| MÉTODO  | URL | ACCIÓN |
| --------------|-------------- |-------------- |
| GET	| `/users`	| Ver todos los platos |
| POST | `/users`  | Crear un usuario  |
| DELETE  | `/users/:id`  | Eliminar un usuario  |

## Credenciales

Creamos un nuevo archivo en la raíz de nuestro proyecto que llamaremos .env en el que editaremos nuestras variables de entorno, el puerto en el que correrá el servidor, el host de nuestra base de datos, el usuario y la contraseña de la base de datos y por último el nombre de la base de datos que previamente habremos creado.

```
PORT=3300
DBHOST=localhost
DBUSER=root
DBPASS=root
DBNAME=nodedb
```


## Correr el proyecto

Para correr el proyecto primero se debe actualizar los paquetes de dependencias

```
npm install
```
Luego se debe instalar de manera global la libreia `nodemon` para poder montar el servidor

```npm install -g nodemon```

Por ultimo para ejecutar el programa utilizamos la instruccion:
```
nodemon index.jsx
```
## Repositorio


[CRUD NodeJS + MySQL](https://gitlab.com/willyjinx96/inf-272)

